from datetime import datetime
import urllib.request
from bs4 import BeautifulSoup
import sys
#from colorama import init, Fore, Back, Style

#init() #colorama

aResp = urllib.request.urlopen("http://www.matikum.se/")
page = aResp.read()
soup = BeautifulSoup(page)
qq = soup.find("div",id="k2ModuleBox88").find("div",class_="moduleItemIntrotext").find_all("ul")
weekday =  datetime.now().weekday()

if weekday < len(qq):
	foodlist = qq[weekday].find_all("li")
else:
	foodlist = [];

for food in foodlist:
	# Replace unprintable characters and print the food.
	# There must be a better way to do this
	print(food.text.replace('\u2013','-').replace('\u201d','"'))


